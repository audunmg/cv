LATEXMK = $(shell command -v latexmk)
ifeq (, $(LATEXMK))
LATEXMK := podman run --rm --volume $(PWD):/srv --workdir /srv --entrypoint latexmk docker.io/aergus/latex:2023-11-15
endif



.PHONY: all
all: gangstoa_cv.pdf

.PHONY: clean
clean:
	rm gangstoa_cv.aux gangstoa_cv.fls gangstoa_cv.log gangstoa_cv.out gangstoa_cv.pdf gangstoa_cv.fdb_latexmk

%.pdf: %.tex
	$(LATEXMK) -pdf
